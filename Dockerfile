FROM dorowu/ubuntu-desktop-lxde-vnc

# install required packages
RUN apt-get clean
RUN apt-get update
RUN apt-get -y install git ant openjdk-8-jdk locales

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8

# fetch robocup simulator
RUN git clone https://github.com/roborescue/rcrs-server.git

# build
WORKDIR rcrs-server
RUN ant clean
RUN ant

# run
WORKDIR boot
#ENTRYPOINT ["./start.sh"]
#CMD ["../maps/gml/test"]
#RUN ./start.sh ../maps/gml/test