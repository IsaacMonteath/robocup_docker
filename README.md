# Requirements
- Docker

# Installation
Clone this repository  
`git clone https://bitbucket.org/IsaacMonteath/robocup_docker.git`  
`cd robocup_docker`

Build the docker image
`docker build -t robocup .`

Start the container (may need to wait for up to a minute)
`docker run -p 6080:80 robocup`

# Usage
Open a web browser to access VNC  
[http://localhost:6080]

Open terminal (in VNC) and start the simulator  
`cd rcrs-server/boot`  
`./start.sh ../maps/gml/paris`